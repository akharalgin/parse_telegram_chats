from telethon.sync import TelegramClient
import csv

# Parse users
from telethon.tl.functions.messages import GetDialogsRequest
from telethon.tl.types import InputPeerEmpty

# Parse messages
from telethon.tl.functions.messages import GetHistoryRequest
from telethon.tl.types import PeerChannel

# Parse docs urls
from telethon.tl.types import MessageEntityTextUrl
import re
from urllib.parse import urlparse

# Parse secrets from .env
from dotenv import load_dotenv
import os

load_dotenv()

# Collect TG secrets from .env
api_id = os.getenv("api_id")
api_hash = os.getenv("api_hash")
phone = os.getenv("phone")
 
 # Start TG client
client = TelegramClient(phone, api_id, api_hash)

client.start()

# Parse chats
chats = []
last_date = None
size_chats = 200
groups=[]

result = client(GetDialogsRequest(
            offset_date=last_date,
            offset_id=0,
            offset_peer=InputPeerEmpty(),
            limit=size_chats,
            hash = 0
        ))
chats.extend(result.chats)

for chat in chats:
   try:
       if chat.megagroup== True:
           groups.append(chat)
   except:
       continue
   
print('Выберите номер группы из перечня:')
i=0
for g in groups:
   print(str(i) + '- ' + g.title)
   i+=1

g_index = input("Введите нужную цифру: ")
target_group=groups[int(g_index)]

# Parse users
"""
print('Узнаём пользователей...')
all_participants = []
all_participants = client.get_participants(target_group)
 
print('Сохраняем данные в файл...')
with open("members.csv","w",encoding='UTF-8') as f:
   writer = csv.writer(f,delimiter=",",lineterminator="\n")
   writer.writerow(['username','name','group'])
   for user in all_participants:
       if user.username:
           username= user.username
       else:
           username= ""
       if user.first_name:
           first_name= user.first_name
       else:
           first_name= ""
       if user.last_name:
           last_name= user.last_name
       else:
           last_name= ""
       name= (first_name + ' ' + last_name).strip()
       writer.writerow([username,name,target_group.title])     
print('Парсинг участников группы успешно выполнен.')
"""

# Message parser
all_messages = []
all_messages_machine = []
offset_id = 0
limit = 100
total_messages = 0
total_count_limit = 0

while True:
   history = client(GetHistoryRequest(
       peer=target_group,
       offset_id=offset_id,
       offset_date=None,
       add_offset=0,
       limit=limit,
       max_id=0,
       min_id=0,
       hash=0
   ))
   if not history.messages:
       break
   messages = history.messages
   for message in messages:
       all_messages.append(message.message) # Добавим параметр message к методу message, чтобы вывести человекочитаемый текст
       all_messages_machine.append(message.to_dict()) # Выводим в машиночитаемом виде
   offset_id = messages[len(messages) - 1].id
   if total_count_limit != 0 and total_messages >= total_count_limit:
       break

print("Сохраняем данные в файл...") #Cообщение для пользователя о том, что начался парсинг сообщений.

### Save data to files
# Save humanreadable data to files
with open(str(target_group.title) + ".csv", "w", encoding="UTF-8") as f:
   writer = csv.writer(f, delimiter=",", lineterminator="\n")
   for message in all_messages:
       writer.writerow([message])     

# Save machinereadable data to files
with open(str(target_group.title) + "_machine.csv", "w", encoding="UTF-8") as f:
   writer = csv.writer(f, delimiter=",", lineterminator="\n")
   for message in all_messages_machine:
       writer.writerow([message])     
print("Парсинг сообщений группы успешно выполнен.") #Сообщение об удачном парсинге чата.

# Parse docs urls
with open(str(target_group.title) + "_machine.csv", mode = "r", encoding = "utf-8") as f_input, open(str(target_group.title) + "_docs_urls.csv", mode = "w", encoding = "utf-8") as f_output:
        reader = csv.reader(f_input)
        writer = csv.writer(f_output)
        
        # Цикл по строкам в исходном CSV-файле
        for row in reader:
            for cell in row:
                # Используем регулярное выражение для поиска ID-сообщения и HTTPS-ссылок
                ids = re.findall(r'\#id \S+', cell)
                links = re.findall(r'https?://\S+', cell)
                # Записываем найденные ссылки в новый файл
                for id in ids:
                    writer.writerow([id])
                    for link in links:
                        parsed_url = urlparse(link)
                        full_path = parsed_url.geturl()
                        writer.writerow([full_path])
